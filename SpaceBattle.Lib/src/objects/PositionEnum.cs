using System.Collections;
using Hwdtech;

namespace SpaceBattle.Lib;

public class PositionEnum : IEnumerator<object>
{
    int distance = IoC.Resolve<int>("Game.Get.Distance.Ships.Opposition");
    List<Vector> positions = IoC.Resolve<List<Vector>>("Game.Get.Positions.Ship");
    int tempShipIndex = IoC.Resolve<int>("Game.Get.Index.TempShip");

    public object Current => positions[tempShipIndex] + new Vector(new int[] {0, distance});

    public bool MoveNext() {

        if (tempShipIndex < positions.Count() - 1) {

            tempShipIndex += 1;
            return true;
        }

        return false;
    }

    public void Dispose() {

        GC.SuppressFinalize(this);
    }

    public void Reset() {

        tempShipIndex = IoC.Resolve<int>("Game.Get.Index.TempShip");
    }
}
