using System.Collections.Concurrent;
namespace SpaceBattle.Lib;
public class SenderAdapter : ISender{
    BlockingCollection<Hwdtech.ICommand> queue;
    public SenderAdapter(BlockingCollection<Hwdtech.ICommand> queue) {
        this.queue = queue;
    }
    public void Send(Hwdtech.ICommand cmd) {
        queue.Add(cmd);
    }
}
