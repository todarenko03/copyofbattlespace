using Scriban;

namespace SpaceBattle.Lib;

public class Builder : IBuilder {

    Type adaptive_class;
    Type adaptable_class;

    string adapterTemplate = @"
using Hwdtech;

namespace SpaceBattle.Lib;

public class {{ adaptive_сlass.name }}Adapter : {{ adaptive_сlass.name }}
{
    public {{ adaptable_сlass.name }} obj;

    public {{ adaptive_сlass.name }}Adapter({{ adaptable_сlass.name }} obj)
    {
        this.obj = obj;
    }{{ '\n' }}
    {{- for prop in properties }}
    public {{ prop.property_type.name }} {{ prop.name }}
    { {{ if prop.can_read }}{{ '\n' }}        get { return IoC.Resolve<object>(""Game.Get.{{ prop.name }}"", obj); } {{ end }} {{ if prop.can_write }}{{ '\n' }}        set { IoC.Resolve<ICommand>(""Game.Set.{{ prop.name }}"", obj, value).Execute(); }{{ end }}
    }{{ '\n' }}
    {{- end}}
}{{ '\n' }}
";

    public Builder (Type adaptiveClass, Type adaptableClass) {

        this.adaptive_class = adaptiveClass;
        this.adaptable_class = adaptableClass;
    }

    public object Build() {

        var properties = adaptive_class.GetProperties();
        var template = Template.Parse(adapterTemplate);
        var res = template.Render(new { adaptive_сlass = adaptive_class, adaptable_сlass = adaptable_class, properties = properties });

        return res;
    }
}
