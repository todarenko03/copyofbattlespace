using System.Collections.Generic;

namespace SpaceBattle.Lib {
    public class PropertiesDelete : ICommand {
        private IPropertiesDeletable _propertiesDeletable;
        private IUObject _uobject; 
        private IList<string> _properties;

        public PropertiesDelete(IPropertiesDeletable propertiesDeletable, IUObject uobject, IList<string> properties) {
            _propertiesDeletable = propertiesDeletable;
            _uobject = uobject;
            _properties = properties;
        }

        public void Execute() {
            _propertiesDeletable.DeleteProperties(_uobject, _properties);
        }
    }
}
