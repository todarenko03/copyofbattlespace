public interface IMessage {
    public string orderType { get; set; }
    public string gameID { get; set; }
    public string gameItemID { get; set; }
    public List<string> listOfProperties { get; set; }
}
