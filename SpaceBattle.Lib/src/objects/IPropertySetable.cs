using System.Collections.Generic;

namespace SpaceBattle.Lib {
    public interface IPropertiesSetable {
        public void SetProperties(IUObject uobject, IList<string> properties);
    }
}
