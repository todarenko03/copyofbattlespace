namespace SpaceBattle.Lib;

public interface IReceiver {
    public Hwdtech.ICommand Receive();
    public bool IsEmpty();
}
