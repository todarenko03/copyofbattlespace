using System.Collections.Generic;

namespace SpaceBattle.Lib {
    public class PropertiesSet : ICommand {
        private IPropertiesSetable _propertiesSetable;
        private IUObject _uobject;
        private IList<string> _properties;

        public PropertiesSet(IPropertiesSetable propertiesSetable, IUObject uobject, IList<string> properties) {
            _propertiesSetable = propertiesSetable;
            _uobject = uobject;
            _properties = properties;
        }

        public void Execute() {
            _propertiesSetable.SetProperties(_uobject, _properties);
        }
    }
}
