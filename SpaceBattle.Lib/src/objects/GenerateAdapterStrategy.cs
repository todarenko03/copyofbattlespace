namespace SpaceBattle.Lib;

public class GenerateAdapterStrategy : IStrategy {

    public object Execute(params object[] args) {

        var adaptive_сlass = (Type)args[0];
        var adaptable_class = (Type)args[1];
        IBuilder builder = new Builder(adaptive_сlass, adaptable_class);
        var res = builder.Build();

        return res;
    }
}    
