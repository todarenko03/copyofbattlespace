using System.Collections.Concurrent;
namespace SpaceBattle.Lib;
public class ReceiverAdapter : IReceiver{
    BlockingCollection<Hwdtech.ICommand> queue;
    public ReceiverAdapter(BlockingCollection<Hwdtech.ICommand> queue){
        this.queue = queue;
    }
    public Hwdtech.ICommand Receive() {
        return queue.Take();
    }
    public bool IsEmpty() {
        if (queue.Count == 0) {
            return true;
        }
        return false;
    }
} 
