namespace SpaceBattle.Lib;

public interface IBuilder {

    public object Build(); 
}
