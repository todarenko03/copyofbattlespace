namespace SpaceBattle.Lib;

public interface ISender {
    public void Send(Hwdtech.ICommand cmd);
}
