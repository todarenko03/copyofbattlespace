using System.Collections.Generic;

namespace SpaceBattle.Lib {
    public interface IPropertiesDeletable {
        public void DeleteProperties(IUObject uobject, IList<string> properties);
    }
}
