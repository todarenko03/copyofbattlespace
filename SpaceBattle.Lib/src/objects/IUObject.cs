namespace SpaceBattle.Lib {
    public interface IUObject {
        public void SetProperty(string key, object property);
        public object GetProperty(string key);
    }
}
