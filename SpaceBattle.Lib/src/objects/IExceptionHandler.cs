namespace SpaceBattle.Lib;

public interface IExceptionHandler {
    public void Execute(ICommand cmd, Exception exception);
}
