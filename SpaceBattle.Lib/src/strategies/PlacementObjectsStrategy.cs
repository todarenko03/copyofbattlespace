using Hwdtech;

namespace SpaceBattle.Lib;

public class PlacementObjectsStrategy : IStrategy {
    public object Execute(params object[] args) {
        
        var shipsID = (IList<string>) args[0];

        return new PlacementObjectsCommand(shipsID);
    }
}
