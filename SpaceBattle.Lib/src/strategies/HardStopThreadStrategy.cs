namespace SpaceBattle.Lib;
public class HardStopThreadStrategy : IStrategy {
    public object Execute(params object[] args) {     
        var id = (string)args[0];
        var action = new ActionCommand(() => {});
        if (args.Length > 1) {
            action = (ActionCommand)args[1];
        }       
        return new SendCommand(id, new HardStopServerCommand(id, action));
    }
}
