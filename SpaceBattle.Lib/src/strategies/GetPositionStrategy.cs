namespace SpaceBattle.Lib {
    public class GetPositionStrategy: IStrategy {
        public object Execute(params object[] args) {
            var uobject = (IUObject)args[0];
            return uobject.GetProperty("Position");
        }
    }
}
