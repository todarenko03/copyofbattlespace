
namespace SpaceBattle.Lib.Server;

public class ProcessingCommandFromHTTPStrategy : IStrategy {
    public object Execute(params object[] args) {

        IMessage cmd = (IMessage)args[0];

        return new ProcessingCommandFromHTTPCommand(cmd);
    }
}
