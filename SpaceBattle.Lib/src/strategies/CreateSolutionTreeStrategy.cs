using System.Collections.Generic;

namespace SpaceBattle.Lib {
    public class CreateSolutionTreeStrategy : IStrategy {
        public object Execute(params object[] args) {
            var propertiesA = (List<int>)args[0];
            var propertiesB = (List<int>)args[1];

            var properties = new List<List<int>>();
            properties.Add(propertiesA);
            properties.Add(propertiesB);

            var solutionTree = new Dictionary<int, object>();

            foreach (var property in properties) {
                var temp = solutionTree;
                foreach (var element in property) {
                    temp.TryAdd(element, new Dictionary<int, object>());
                    temp = (Dictionary<int, object>)temp[element];
                }
            }

            return solutionTree;
        }
    }
}
