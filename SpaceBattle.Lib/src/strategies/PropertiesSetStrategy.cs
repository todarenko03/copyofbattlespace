using System.Collections.Generic;

namespace SpaceBattle.Lib {
    public class PropertiesSetStrategy : IStrategy {
        IPropertiesSetable _propertiesSetable;
    
        public PropertiesSetStrategy(IPropertiesSetable propertiesSetable) {
            _propertiesSetable = propertiesSetable;
        }

        public object Execute(params object[] args) {
            var uobject = (IUObject)args[0];
            var properties = (IList<string>)args[1];

            return new PropertiesSet(_propertiesSetable, uobject, properties);
        }
    }
}
