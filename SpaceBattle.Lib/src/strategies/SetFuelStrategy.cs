using Hwdtech;

namespace SpaceBattle.Lib;

public class SetFuelStrategy : IStrategy {
    public object Execute(params object[] args) {
        
        var shipIDForSetting = (string) args[0];
        var fuelValue = (int) args[1];

        return new SetFuelCommand(shipIDForSetting, fuelValue);
    }
}
