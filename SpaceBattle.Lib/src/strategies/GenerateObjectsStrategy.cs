using Hwdtech;

namespace SpaceBattle.Lib;

public class GenerateObjectsStrategy : IStrategy {
    public object Execute(params object[] args) {

        var count = IoC.Resolve<int>("Game.Get.CountOfObjects");
        var objects = Enumerable.Range(1, count).ToDictionary(i => i.ToString(), i => IoC.Resolve<IUObject>("Game.Get.UObject"));

        return objects;
    }
}
