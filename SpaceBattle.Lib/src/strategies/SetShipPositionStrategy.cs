namespace SpaceBattle.Lib;

public class SetShipPositionStrategy : IStrategy {
    public object Execute(params object[] args) {

        var id = (string) args[0];
        var iterator = (IEnumerator<object>) args[1];

        return new SetShipPositionCommand(id, iterator);
    }
}
