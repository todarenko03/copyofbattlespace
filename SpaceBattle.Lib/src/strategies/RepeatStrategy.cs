namespace SpaceBattle.Lib {
    public class RepeatStrategy : IStrategy {
        public object Execute(params object[] args) {
            var uobject = (IUObject)args[0];
            var command = (ICommand)args[1];

            return new RepeatCommand(uobject, command);
        }
    }
}
