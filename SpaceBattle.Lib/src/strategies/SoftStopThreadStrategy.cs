namespace SpaceBattle.Lib;
public class SoftStopThreadStrategy : IStrategy {
    public object Execute(params object[] args) {
        var id = (string)args[0];
        var action = new ActionComand(() => {});
        if (args.Length > 1) {
            action = (ActionComand)args[1];
        }
        return new SendCommand(id, new SoftStopServerCommand(id, action));
    }
}
