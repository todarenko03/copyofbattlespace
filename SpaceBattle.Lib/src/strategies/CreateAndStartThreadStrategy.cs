using Hwdtech;
using Hwdtech.Ioc;
using System.Collections.Concurrent;
namespace SpaceBattle.Lib;
public class CreateAndStartThreadStrategy : IStrategy {
    public object Execute(params object[] args) {
        var id = (string)args[0];
        var threadsPool = IoC.Resolve<ConcurrentDictionary<string, ServerThread>>("Game.Server.GetThreadsPool");
        var sendersPool = IoC.Resolve<ConcurrentDictionary<string, ISender>>("Game.Server.GetSendersPool");
        var action = new ActionCommand(() => {});
        BlockingCollection<Hwdtech.ICommand> queue = new BlockingCollection<Hwdtech.ICommand>(1000);
        var receiver = new ReceiverAdapter(queue);
        var sender = new SenderAdapter(queue);
        var thread =  new ServerThread(receiver);
        if (args.Length > 1) {
            action = (ActionCommand)args[1];
        }
        threadsPool.TryAdd(id, thread);
        sendersPool.TryAdd(id, sender);
        action.Execute();
        return thread;
    }
}
