namespace SpaceBattle.Lib;

public class GetCommandFromQueueStrategy : IStrategy {
    
    public object Execute(params object[] args) {
        
        var queue = (Queue<ICommand>) args[0];

        if (!queue.TryDequeue(out ICommand? cmd)) {

            throw new Exception(); 
        }

        return cmd;
    }
}
