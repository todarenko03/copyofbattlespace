namespace SpaceBattle.Lib;

public class ExecuteGamesCommandsStrategy : IStrategy {
    public object Execute(params object[] args) {

        var queue = (Queue<ICommand>)args[0];

        return new ExecuteGamesCommandsCommand(queue);
    }
}
