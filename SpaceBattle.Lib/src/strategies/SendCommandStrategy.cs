namespace SpaceBattle.Lib;
public class SendCommandStrategy : IStrategy {
    public object Execute(params object[] args) {
        var id = (string)args[0];
        var cmd = (Hwdtech.ICommand)args[1];
        return new SendCommand(id, cmd);
    }
}
