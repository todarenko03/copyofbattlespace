namespace SpaceBattle.Lib {
    public class InjectStrategy : IStrategy {
        public object Execute(params object[] args){
            var uobject = (IUObject)args[0];
            var command = (ICommand)args[1];

            return new InjectCommand(uobject, command);
        }
    }
}
