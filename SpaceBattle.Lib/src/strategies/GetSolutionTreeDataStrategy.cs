using System.Collections.Generic;

namespace SpaceBattle.Lib {
    public class GetSolutionTreeDataStrategy : IStrategy {
        public object Execute(params object[] args) {
            var properties = new List<int>();

            var propertiesA = (List<int>)args[0];
            var propertiesB = (List<int>)args[1];

            foreach (var property in propertiesA) {
                properties.Add(property - propertiesB[propertiesA.IndexOf(property)]);
            }

            return properties;
        }
    }
}
