using System.Collections.Generic;

namespace SpaceBattle.Lib {
    public class PropertiesDeleteStrategy : IStrategy{
        private IPropertiesDeletable _propertiesDeletable;

        public PropertiesDeleteStrategy(IPropertiesDeletable propertiesDeletable){
            _propertiesDeletable = propertiesDeletable;
        }

        public object Execute(params object[] args) {
            var uobject = (IUObject)args[0];
            var properties = (IList<string>)args[1];

            return new PropertiesDelete(_propertiesDeletable, uobject, properties);
        }
    }
}
