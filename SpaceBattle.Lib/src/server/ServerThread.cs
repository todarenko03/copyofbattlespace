namespace SpaceBattle.Lib;

public class ServerThread {
    bool stop = false;
    Thread thread;
    IReceiver queue;
    Action strategy;
    public ServerThread(IReceiver queue) {
        this.queue = queue;
        this.strategy = () => {
            HandleCommand();
        };
        this.thread = new Thread(() => {
            while(!stop) {               
                strategy();
            }
        });
    }
    public void HandleCommand() {
        var cmd = queue.Receive();
        cmd.Execute();
    }
    public void UpdateBehaviour(Action newStrategy) {
        strategy = newStrategy;
    }
    public void Execute() {
        thread.Start();
    }
    public void Stop() {
        stop = true;
    }
    public bool IsEmpty() {
        if (queue.IsEmpty()) {
            return true;
        }
        return false;
    }
    public static bool operator == (ServerThread st, Thread t) {
        return st.thread == t;
    }
    public static bool operator != (ServerThread st, Thread t) {
        return !(st.thread == t);
    }
    public override int GetHashCode() {
        return this.thread.GetHashCode();
    }
    public override bool Equals(object? obj) {
        return obj is Thread th && th == this.thread;
    }
}
