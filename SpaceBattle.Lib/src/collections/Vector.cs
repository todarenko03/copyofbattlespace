namespace SpaceBattle.Lib {
    public class Vector {
        private int[] _array;
        private int _size;

        public Vector(params int[] array) {
            _size = array.Length;
            _array = new int[_size];

            for (int i = 0; i < _size; i++) {
                _array[i] = array[i];
            }
        }

        public override bool Equals(object? obj) {
            return obj is Vector;
        }

        public override int GetHashCode() {
    
            unchecked
            {
                int hash = (int)2166136261;
                _array.ToList().ForEach(n => hash = (hash * 16777619) ^ n.GetHashCode());
                return hash;
            }

        }

        public int this[int index] {
            get {
                return _array[index];
            }

            set {
                _array[index] = value;
            }
        }

        public int Size() {
            return _size;
        }

        public static Vector operator+(Vector a, Vector b) {
            if (a.Size() != b.Size()) { 
                throw new ArgumentException();
            } else {
                var new_array = new int[a.Size()];

                for (int i = 0; i < a.Size(); i++) {
                    new_array[i] = a[i] + b[i];
                }

                return new Vector(new_array);
            }
        }

        public static Vector operator-(Vector a, Vector b) {
            if (a.Size() != b.Size()) { 
                throw new ArgumentException();
            } else {
                var new_array = new int[a.Size()];

                for (int i = 0; i < a.Size(); i++) {
                    new_array[i] = a[i] - b[i];
                }

                return new Vector(new_array);
            }
        }
    }
}
