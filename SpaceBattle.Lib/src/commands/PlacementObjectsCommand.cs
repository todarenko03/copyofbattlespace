using System.Collections;
using Hwdtech;

namespace SpaceBattle.Lib;

public class PlacementObjectsCommand : ICommand {

    IList<string> shipsID;
    public PlacementObjectsCommand(IList<string> shipsID) {

        this.shipsID = shipsID;
    }

    public void Execute() {

        var iterator = IoC.Resolve<IEnumerator>("Game.Get.PositionEnum");

        foreach (var id in shipsID) {

            IoC.Resolve<ICommand>("Game.Set.Ship.Position", id, iterator).Execute();
        }

        iterator.Reset();
    }
}
