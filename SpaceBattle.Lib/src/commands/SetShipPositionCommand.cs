using Hwdtech;

namespace SpaceBattle.Lib;

public class SetShipPositionCommand : ICommand {

    string id;

    IEnumerator<object> iterator;
    public SetShipPositionCommand(string id, IEnumerator<object> iterator) {

        this.id = id;
        this.iterator = iterator;
    }

    public void Execute() {

        var distance = IoC.Resolve<int>("Game.Get.Distance.Ships.Team");
        var objects = IoC.Resolve<IDictionary<string, IUObject>>("Game.Get.Objects");
        var koef = IoC.Resolve<int>("Game.Get.Coefficient.ShipsPlace");

        objects[id].SetProperty("Position", (Vector) iterator.Current + new Vector(new int[] {0, koef * distance}));

        iterator.MoveNext();
    }
}
