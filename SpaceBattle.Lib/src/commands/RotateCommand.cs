namespace SpaceBattle.Lib {
    public class RotateCommand : ICommand {
        private IRotatable _rotatable;

        public RotateCommand(IRotatable rotatable) {
            _rotatable = rotatable;
        }

        public void Execute() {
            _rotatable.Angle += _rotatable.AngularVelocity;
        }
    }
}
