using System.Collections.Generic;

using Hwdtech;
using Hwdtech.Ioc;

namespace SpaceBattle.Lib {
    public class CheckCollisionCommand : ICommand {
        private IUObject _uobjectA;
        private IUObject _uobjectB;

        public CheckCollisionCommand(IUObject uobjectA, IUObject uobjectB) {
            _uobjectA = uobjectA;
            _uobjectB = uobjectB;
        }

        public void Execute() {
            var propertiesA = IoC.Resolve<List<int>>("Game.Object.GetPosition", _uobjectA);
            var propertiesB = IoC.Resolve<List<int>>("Game.Object.GetPosition", _uobjectB);

            var solutionTree = IoC.Resolve<Dictionary<int, object>>("Game.Collision.CreateSolutionTree", propertiesA, propertiesB);
            var properties = IoC.Resolve<List<int>>("Game.Collision.GetSolutionTreeData", propertiesA, propertiesB);

            foreach (var property in properties) {
                if (!solutionTree.TryGetValue(property, out object? value)) {
                    return;
                }
                solutionTree = (Dictionary<int, object>)value;
            }

            IoC.Resolve<ICommand>("Game.Collision.CollisionFound", _uobjectA, _uobjectB).Execute();
        }
    }
}
