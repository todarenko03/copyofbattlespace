namespace SpaceBattle.Lib {
    public interface IRepeatable {
        public ICommand Repeat();
    }
}
