using Hwdtech;
using Hwdtech.Ioc;

namespace SpaceBattle.Lib;

public class GameCommand: ICommand {

    private Queue<ICommand> queue;
    private object scope;

    public GameCommand(Queue<ICommand> queue, object scope) {
        this.queue = queue;
        this.scope = scope;
    }

    public void Execute() {
        IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", scope).Execute();
        IoC.Resolve<ICommand>("Game.Execute.GamesCommands", queue).Execute();
    }
}
