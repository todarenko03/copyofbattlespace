using Hwdtech;
using Hwdtech.Ioc;
using System.Collections.Concurrent;
namespace SpaceBattle.Lib;
public class SoftStopServerCommand : Hwdtech.ICommand {
    ServerThread stoppingThread;
    string id;
    ActionComand action;
    public SoftStopServerCommand(string id, ActionComand action) {
        this.id = id;
        var threadsPool = IoC.Resolve<ConcurrentDictionary<string, ServerThread>>("Game.Server.GetThreadsPool");
        stoppingThread = threadsPool[id];
        this.action = action;
    }
    public void SoftStopAction() {
        if (stoppingThread.IsEmpty()) {
            stoppingThread.Stop();
            action.Execute();
        }
        else {
            stoppingThread.HandleCommand();
        }
    }
    public void Execute() {
        if (stoppingThread != Thread.CurrentThread) {
            throw new Exception();
        }        
        var softStopAction = () => {SoftStopAction();};
        stoppingThread.UpdateBehaviour(softStopAction);
        SoftStopAction();
    }
}
