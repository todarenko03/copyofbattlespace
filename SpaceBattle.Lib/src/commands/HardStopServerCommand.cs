using Hwdtech;
using Hwdtech.Ioc;
using System.Collections.Concurrent;
namespace SpaceBattle.Lib;
public class HardStopServerCommand : Hwdtech.ICommand {
    ServerThread stoppingThread;
    ActionCommand action;
    public HardStopServerCommand(string id, ActionCommand action) {
        var threadsPool = IoC.Resolve<ConcurrentDictionary<string, ServerThread>>("Game.Server.GetThreadsPool");
        stoppingThread = threadsPool[id];
        this.action = action; 
    }  
    public void Execute() {
        if (stoppingThread != Thread.CurrentThread) {
            throw new Exception();
        }        
        stoppingThread.Stop();
        action.Execute();
    }
}
