namespace SpaceBattle.Lib;

public class ActionComand : Hwdtech.ICommand {
    Action action;

    public ActionComand (Action action) {

        this.action = action;
    }

    public void Execute() {
        action();
    }
}
