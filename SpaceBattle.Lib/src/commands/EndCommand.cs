using Hwdtech;
using Hwdtech.Ioc;

namespace SpaceBattle.Lib {
    public class EndCommand : ICommand {
        private IEndable _endable;

        public EndCommand(IEndable endable) {
            _endable = endable;
        }
    
        public void Execute(){
            var uobject = _endable.UObject;
            var command = _endable.Command;
            var properties = _endable.Properties;

            IoC.Resolve<ICommand>("Game.Object.DeleteProperties", uobject, properties).Execute();
            IoC.Resolve<ICommand>("Game.Queue.Push", IoC.Resolve<IInjectable>("Game.Commands.Inject", uobject, command).Inject());
        }
    }
}
