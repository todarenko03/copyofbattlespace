namespace SpaceBattle.Lib;

public class RepeatCommand : IRepeatable {
    private IUObject _uobject;
    private ICommand _command;

    public RepeatCommand(IUObject uobject, ICommand command) {
        _uobject = uobject;
        _command = command;
    }

    public ICommand Repeat() {
        return _command;
    }
}
