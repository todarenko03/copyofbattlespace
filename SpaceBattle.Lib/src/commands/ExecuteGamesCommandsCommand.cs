using Hwdtech.Ioc;
using Hwdtech;
using System.Globalization;
using System.Diagnostics;

namespace SpaceBattle.Lib;

public class ExecuteGamesCommandsCommand : ICommand {

    public Queue<ICommand> queue;
    public ExecuteGamesCommandsCommand(Queue<ICommand> queue) {

        this.queue = queue;
    }

    public void Execute() {

        var stopWatch = new Stopwatch();

        stopWatch.Start();

        while(stopWatch.Elapsed.TotalMilliseconds <= IoC.Resolve<double>("Game.Get.QuantumOfTime")) {
            var cmd = IoC.Resolve<ICommand>("Game.Get.CommandFromQueue", queue);

            try {

                cmd.Execute();
            }
            
            catch(Exception exception) {

                exception.Data["typeOf"] = cmd.GetType();
                IoC.Resolve<IHandler>("Game.Get.ExceptionHandler", exception).Handle();
            }
        }

        stopWatch.Stop();
    }
}
