namespace SpaceBattle.Lib {
    public interface IStartable {
        public IUObject UObject {
            get;
        }

        public ICommand Command {
            get;
        }

        public IList<string> Properties {
            get;
        }
    }
}
