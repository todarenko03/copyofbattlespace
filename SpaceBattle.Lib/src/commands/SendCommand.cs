using Hwdtech;
using Hwdtech.Ioc;
using System.Collections.Concurrent;
namespace SpaceBattle.Lib;
public class SendCommand : Hwdtech.ICommand{
    string id;
    Hwdtech.ICommand cmd;
    ISender sender;
    public SendCommand(string id, Hwdtech.ICommand cmd) {
        this.id = id;
        this.cmd = cmd;
        var sendersPool = IoC.Resolve<ConcurrentDictionary<string, ISender>>("Game.Server.GetSendersPool");
        sender = sendersPool[id];
    }
    public void Execute() {
        sender.Send(cmd);
    }
}
