using Hwdtech;
using Hwdtech.Ioc;

namespace SpaceBattle.Lib {
    public class StartCommand : ICommand {
        private IStartable _startable;

        public StartCommand(IStartable startable) {
            _startable = startable;
        }

        public void Execute() {
            var uobject = _startable.UObject;
            var command = _startable.Command;
            var properties = _startable.Properties;

            IoC.Resolve<ICommand>("Game.Object.SetProperties", uobject, properties).Execute();
            IoC.Resolve<ICommand>("Game.Queue.Push", IoC.Resolve<IRepeatable>("Game.Commands.Repeat", uobject, command).Repeat());
        }
    }
}
