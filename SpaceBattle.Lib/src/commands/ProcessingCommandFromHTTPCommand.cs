using Hwdtech;
using SpaceBattle.Lib.Server;

namespace SpaceBattle.Lib;

public class ProcessingCommandFromHTTPCommand : ICommand {
    
    IMessage cmd;
    public ProcessingCommandFromHTTPCommand(IMessage cmd) {
        this.cmd = cmd;
    }
    public void Execute() {
        var threads = IoC.Resolve<IDictionary<string, string>>("GetServerThreads");
        var mc = IoC.Resolve<ICommand>("Game.Create.MacroCommand");
        IoC.Resolve<ICommand>("Game.Server.SendMessage", threads[(string)cmd.gameID], mc).Execute();
    }
}
