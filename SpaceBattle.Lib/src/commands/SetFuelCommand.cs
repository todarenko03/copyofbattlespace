using Hwdtech;

namespace SpaceBattle.Lib;

public class SetFuelCommand : ICommand {

    string shipIDForSetting;
    int fuelValue;

    public SetFuelCommand(string shipIDForSetting, int fuelValue) {

        this.shipIDForSetting = shipIDForSetting;
        this.fuelValue = fuelValue;
    }

    public void Execute() {

        var objects = IoC.Resolve<IDictionary<string, IUObject>>("Game.Get.Objects");

        IoC.Resolve<ICommand>("Game.Set.Property.UObject", "Fuel", objects[shipIDForSetting], fuelValue);
    }
}
