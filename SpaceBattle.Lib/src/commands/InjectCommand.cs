namespace SpaceBattle.Lib {
    public class InjectCommand : IInjectable{
        private IUObject _uobject;
        private ICommand _command;

        public InjectCommand(IUObject uobject, ICommand command){
            _uobject = uobject;
            _command = command;
        }

        public ICommand Inject() { 
            _command = new EmptyCommand();
            return _command;
        }
    }
}
