namespace SpaceBattle.Lib {
    public interface IEndable {
        public IUObject UObject {
            get;
        }

        public ICommand Command {
            get;
        }
        
        public IList<string> Properties {
            get;
        } 
    }
}
