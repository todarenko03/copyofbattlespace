namespace SpaceBattle.Lib {
    public interface IInjectable {
        public ICommand Inject();
    }
}
