using System.Net;
using CoreWCF;
using CoreWCF.OpenApi.Attributes;
using CoreWCF.Web;

namespace SpaceBattle.Lib.Server
{
    [ServiceContract]
    [OpenApiBasePath("/api")]
    internal interface IWebAPI
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/body")]
        [OpenApiTag("Tag")]
        [OpenApiResponse(ContentTypes = new[] { "application/json"}, Description = "Success", StatusCode = HttpStatusCode.OK, Type = typeof(CommandContract)) ]
        CommandContract ProcessingCommandFromHTTP(
            [OpenApiParameter(ContentTypes = new[] { "application/json"}, Description = "param description.")] CommandContract param);
    }
}
