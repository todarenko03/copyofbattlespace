using System.Collections.Generic;
using System.Runtime.Serialization;
using CoreWCF.OpenApi.Attributes;

namespace SpaceBattle.Lib.Server
{
    ///<Summary>
    /// СommandContract
    ///</Summary>
    [DataContract(Name = "CommandContract")]
    public class CommandContract: IMessage
    {
        ///<Summary>
        /// orderType
        ///</Summary>   
        [DataMember(Name = "OrderType", Order = 1)]
        [OpenApiProperty(Description = "Тип команды")]
        public string orderType { get; set; }

        ///<Summary>
        /// gameID
        ///</Summary>
        [DataMember(Name = "GameID", Order = 1)]
        [OpenApiProperty(Description = "ID игры")]
        public string gameID { get; set; }

        ///<Summary>
        /// gameItemID
        ///</Summary>
        [DataMember(Name = "GameItemID", Order = 2)]
        [OpenApiProperty(Description = "ID объекта")]
        public string gameItemID { get; set; }
        
        ///<Summary>
        /// listOfProperties
        ///</Summary>
        [DataMember(Name = "Properties", Order = 3)]
        [OpenApiProperty(Description = "Свойства объекта")]
        public List<string> listOfProperties { get; set; }
    }
}
