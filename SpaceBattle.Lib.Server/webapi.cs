using CoreWCF;
using Hwdtech;

namespace SpaceBattle.Lib.Server
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    internal class WebAPI : IWebAPI
    {
        public CommandContract ProcessingCommandFromHTTP(CommandContract param){
            IoC.Resolve<Hwdtech.ICommand>("ProcessingCommandFromHTTP", param).Execute();
            return param;
        }
    }
}
