using System;
using System.Collections.Concurrent;

using Xunit;
using Moq;

using Hwdtech;
using Hwdtech.Ioc;
using System.Threading;

namespace SpaceBattle.Lib.Test;

public class SoftStopThreadStrategyTest {
    public SoftStopThreadStrategyTest() {
        var threadPool = new ConcurrentDictionary<string, ServerThread>();
        var senderPool = new ConcurrentDictionary<string, ISender>();
        
        new InitScopeBasedIoCImplementationCommand().Execute();
        IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"))).Execute();

        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Server.GetThreadsPool", (object[] args) => threadPool).Execute();
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Server.GetSendersPool", (object[] args) => senderPool).Execute();
    }
    [Fact]
    public void softStopThreadStrategyTest1() {
        var id = "11";
        BlockingCollection<Hwdtech.ICommand> queue = new BlockingCollection<Hwdtech.ICommand>(1000);
        var receiver = new ReceiverAdapter(queue);
        var thread =  new ServerThread(receiver);
        var sender = new SenderAdapter(queue);
        thread.Execute();

        var sendersPool = IoC.Resolve<ConcurrentDictionary<string, ISender>>("Game.Server.GetSendersPool");
        sendersPool.TryAdd(id, sender);

        var threadsPool = IoC.Resolve<ConcurrentDictionary<string, ServerThread>>("Game.Server.GetThreadsPool");
        threadsPool.TryAdd(id, thread);

        var sss = (Hwdtech.ICommand) new SoftStopThreadStrategy().Execute(id);
        sss.Execute();
    }

    [Fact]
    public void softStopThreadStrategyTest2() {
        var id = "10";
        BlockingCollection<Hwdtech.ICommand> queue = new BlockingCollection<Hwdtech.ICommand>(1000);
        var receiver = new ReceiverAdapter(queue);
        var thread =  new ServerThread(receiver);
        var sender = new SenderAdapter(queue);
        var are = new AutoResetEvent(false);
        var action = new ActionComand(() => {are.Set();});
        thread.Execute();

        var sendersPool = IoC.Resolve<ConcurrentDictionary<string, ISender>>("Game.Server.GetSendersPool");
        sendersPool.TryAdd(id, sender);

        var threadsPool = IoC.Resolve<ConcurrentDictionary<string, ServerThread>>("Game.Server.GetThreadsPool");
        threadsPool.TryAdd(id, thread);

        var sss = (Hwdtech.ICommand) new SoftStopThreadStrategy().Execute(id, action);
        sss.Execute();

        are.WaitOne();

        Assert.True(thread.IsEmpty());
    }

    [Fact]
    public void softStopThreadStrategyTest3() {
        var id = "10";
        BlockingCollection<Hwdtech.ICommand> queue = new BlockingCollection<Hwdtech.ICommand>(1000);
        var receiver = new ReceiverAdapter(queue);
        var thread =  new ServerThread(receiver);
        var sender = new SenderAdapter(queue);
        var are = new AutoResetEvent(false);
        var action = new ActionComand(() => {are.Set();});

        var sendersPool = IoC.Resolve<ConcurrentDictionary<string, ISender>>("Game.Server.GetSendersPool");
        sendersPool.TryAdd(id, sender);

        var threadsPool = IoC.Resolve<ConcurrentDictionary<string, ServerThread>>("Game.Server.GetThreadsPool");
        threadsPool.TryAdd(id, thread);
        
        Assert.True(thread.IsEmpty());

        thread.Execute();

        var scs1 = (Hwdtech.ICommand) new SendCommandStrategy().Execute(id, new Mock<Hwdtech.ICommand>().Object);
        scs1.Execute();

        var scs2 = (Hwdtech.ICommand) new SendCommandStrategy().Execute(id, new Mock<Hwdtech.ICommand>().Object);
        scs2.Execute();

        var sss = (Hwdtech.ICommand) new SoftStopThreadStrategy().Execute(id, action);
        sss.Execute();

        var scs3 = (Hwdtech.ICommand) new SendCommandStrategy().Execute(id, new Mock<Hwdtech.ICommand>().Object);
        scs3.Execute();

        are.WaitOne();

        Assert.True(thread.IsEmpty());

        var scs = (Hwdtech.ICommand) new SendCommandStrategy().Execute(id, new Mock<Hwdtech.ICommand>().Object);
        scs.Execute();

        Assert.False(thread.IsEmpty());
    }

    [Fact]
    public void UnsuccessfulSoftStopServerThreadCommandBecauseExecuteThrowExceptionWithDifferentThreads()
    {
        var id = "10";

        BlockingCollection<Hwdtech.ICommand> queue = new BlockingCollection<Hwdtech.ICommand>(1000);
        var receiver = new ReceiverAdapter(queue);
        var thread =  new ServerThread(receiver);
        var sender = new SenderAdapter(queue);
        var action = new ActionCommand(() => {});

        var sendersPool = IoC.Resolve<ConcurrentDictionary<string, ISender>>("Game.Server.GetSendersPool");
        sendersPool.TryAdd(id, sender);

        var threadsPool = IoC.Resolve<ConcurrentDictionary<string, ServerThread>>("Game.Server.GetThreadsPool");
        threadsPool.TryAdd(id, thread);

        var ssFlag = false;

        var are = new AutoResetEvent(false);

        var createAndStartSTStrategy = new CreateAndStartThreadStrategy();
        var c = (ServerThread) createAndStartSTStrategy.Execute(id);

        var serverThread = threadsPool[id];
        var ss = new SoftStopServerCommand(id, new ActionComand(() => { 
            ssFlag = true; 
            are.Set();
        }));

        Assert.Throws<Exception>(() => {
            ss.Execute();
            are.WaitOne();
        });

        Assert.False(ssFlag);
    }

     [Fact]
    public void SuccessfulSoftStopCommandExecuteWithOtherCommandsInServerThreadSender()
    {
        var key = "10";
        var isExecute = false;

        BlockingCollection<Hwdtech.ICommand> queue = new BlockingCollection<Hwdtech.ICommand>(1000);
        var receiver = new ReceiverAdapter(queue);
        var thread =  new ServerThread(receiver);
        var sender = new SenderAdapter(queue);
        var action = new ActionCommand(() => {});

        var sendersPool = IoC.Resolve<ConcurrentDictionary<string, ISender>>("Game.Server.GetSendersPool");
        sendersPool.TryAdd(key, sender);

        var threadsPool = IoC.Resolve<ConcurrentDictionary<string, ServerThread>>("Game.Server.GetThreadsPool");
        threadsPool.TryAdd(key, thread);


        var are = new AutoResetEvent(false);

        thread.Execute();
        
        var softStopStrategy = new SoftStopThreadStrategy();
        var sendStrategy = new SendCommandStrategy();

        var ss = (Hwdtech.ICommand)softStopStrategy.Execute(key, new ActionComand(() => 
        {
            are.Set();
        }));
        ss.Execute();

        var c2 = (Hwdtech.ICommand)sendStrategy.Execute(key, new ActionComand(() =>
        {
            isExecute = true;
            are.Set();
        }));
        c2.Execute();
        are.WaitOne();
        are.WaitOne();

        Assert.True(isExecute);
    }
}
