using System;
using System.Collections.Concurrent;

using Xunit;
using Moq;

using Hwdtech;
using Hwdtech.Ioc;

namespace SpaceBattle.Lib.Test;

public class SendCommandStrategyTest {

    public SendCommandStrategyTest() {
        var threadPool = new ConcurrentDictionary<string, ServerThread>();
        var senderPool = new ConcurrentDictionary<string, ISender>();
        
        new InitScopeBasedIoCImplementationCommand().Execute();
        IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"))).Execute();

        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Server.GetThreadsPool", (object[] args) => threadPool).Execute();
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Server.GetSendersPool", (object[] args) => senderPool).Execute();
    }
    [Fact]
    public void sendCommandStrategyTest() {
        var id = "6";
        var cmd = new Mock<Hwdtech.ICommand>();
        var thread = new CreateAndStartThreadStrategy().Execute(id);
        
        var scs = (Hwdtech.ICommand) new SendCommandStrategy().Execute(id, cmd.Object);
        scs.Execute();

        var hardStopStrategy = new HardStopThreadStrategy();
        var hs = (Hwdtech.ICommand)hardStopStrategy.Execute(id);
        hs.Execute();
        
    }
}
