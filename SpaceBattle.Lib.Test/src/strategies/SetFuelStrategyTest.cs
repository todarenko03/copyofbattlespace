using System;
using System.Collections.Generic;

using Xunit;
using Moq;

using Hwdtech;
using Hwdtech.Ioc;

namespace SpaceBattle.Lib.Test;

public class SetFuelStrategyTest {
    [Fact]
    public void successfulSetFuelStrategyTest() {
        
        var shipIDForSetting = "1";
        var fuelValue = 5;
        var sfs = new SetFuelStrategy();
        var sfc = sfs.Execute(shipIDForSetting, fuelValue);
    }
}
