using Xunit;
using Moq;

namespace SpaceBattle.Lib.Test;

public class GenerateAdapterStrategyTest {
    
    [Fact]
    public void generateAdapterStrategyTest() {
        
        var gas = new GenerateAdapterStrategy().Execute(typeof(IMovable), typeof(ICommand));

        var target = @"
using Hwdtech;

namespace SpaceBattle.Lib;

public class IMovableAdapter : IMovable
{
    public ICommand obj;

    public IMovableAdapter(ICommand obj)
    {
        this.obj = obj;
    }

    public Vector Position
    { 
        get { return IoC.Resolve<object>(""Game.Get.Position"", obj); }  
        set { IoC.Resolve<ICommand>(""Game.Set.Position"", obj, value).Execute(); }
    }

    public Vector Velocity
    { 
        get { return IoC.Resolve<object>(""Game.Get.Velocity"", obj); }  
    }

}

";

        Assert.Equal(target, gas);
    }
}
