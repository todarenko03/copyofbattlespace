using System.Threading;
using System.Collections.Concurrent;
using Xunit;
using Moq;
using System;

using Hwdtech;
using Hwdtech.Ioc;
using SpaceBattle.Lib.Server;

namespace SpaceBattle.Lib.Test;

public class ProcessingCommandFromHTTPStrategyTest {

    public ProcessingCommandFromHTTPStrategyTest() {

        new InitScopeBasedIoCImplementationCommand().Execute();
        IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"))).Execute();

        var dict = new ConcurrentDictionary<string, string>();
        dict.TryAdd("1", "1");
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "GetServerThreads", (object[] args) => dict).Execute();
        var sendMessage = new Mock<IStrategy>();
        var cmd = new Mock<ICommand>();
        sendMessage.Setup(x => x.Execute(It.IsAny<object[]>())).Returns(cmd.Object);
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Server.SendMessage", (object[] args) => sendMessage.Object.Execute(args)).Execute();
        var macroCommand = new Mock<IStrategy>();
        macroCommand.Setup(s => s.Execute()).Returns(cmd.Object);
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Create.MacroCommand", (object[] args) => macroCommand.Object.Execute(args)).Execute();
    }
    [Fact]
    public void processingCommandFromHTTPStrategyTest() {
        var cmd = new Mock<IMessage>();
        cmd.SetupGet(x => x.gameID).Returns("1");
        // cmd.Object.gameID = "1";
        var pcfhs = new ProcessingCommandFromHTTPStrategy();
       var cmd2 =  (ICommand)pcfhs.Execute(cmd.Object);
       cmd2.Execute();
        // pcfhs.Execute();
    }
}
