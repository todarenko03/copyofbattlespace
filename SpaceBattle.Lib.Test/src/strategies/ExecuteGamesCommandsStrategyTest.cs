using Moq;
using System.Collections.Generic;
using Xunit;

namespace SpaceBattle.Lib.Test;

public class ExecuteGamesCommandsStrategyTest {
    [Fact]
    public void SuccessfulExecuteGamesCommandsStrategyTest() {
        var queue = new Queue<ICommand>();
        var quant = 5.0;

        var executeGamesCommandsStrategy = new ExecuteGamesCommandsStrategy();

        Assert.NotNull(executeGamesCommandsStrategy.Execute(queue, quant));
    }
}
