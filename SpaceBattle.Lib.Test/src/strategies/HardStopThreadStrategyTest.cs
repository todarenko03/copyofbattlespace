using System;
using System.Collections.Concurrent;

using Xunit;
using Moq;

using Hwdtech;
using Hwdtech.Ioc;

namespace SpaceBattle.Lib.Test;

public class HardStopThreadStrategyTest {

    ConcurrentDictionary<string, ServerThread> threadPool = new ConcurrentDictionary<string, ServerThread>();
    ConcurrentDictionary<string, ISender> senderPool = new ConcurrentDictionary<string, ISender>();

    public HardStopThreadStrategyTest() {
        new InitScopeBasedIoCImplementationCommand().Execute();
        IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"))).Execute();

        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Server.GetThreadsPool", (object[] args) => threadPool).Execute();
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Server.GetSendersPool", (object[] args) => senderPool).Execute();
    }
    [Fact]
    public void hardStopThreadStrategyTest() {
        var id = "10";
        BlockingCollection<Hwdtech.ICommand> queue = new BlockingCollection<Hwdtech.ICommand>(1000);
        var receiver = new ReceiverAdapter(queue);
        var thread =  new ServerThread(receiver);
        var sender = new SenderAdapter(queue);
        thread.Execute();

        var sendersPool = IoC.Resolve<ConcurrentDictionary<string, ISender>>("Game.Server.GetSendersPool");
        sendersPool.TryAdd(id, sender);

        var threadsPool = IoC.Resolve<ConcurrentDictionary<string, ServerThread>>("Game.Server.GetThreadsPool");
        threadsPool.TryAdd(id, thread);

        var hss = (Hwdtech.ICommand) new HardStopThreadStrategy().Execute(id);
        hss.Execute();
    }

    [Fact]
    public void hardStopThreadStrategyTest2() {
        var id = "10";
        BlockingCollection<Hwdtech.ICommand> queue = new BlockingCollection<Hwdtech.ICommand>(1000);
        var receiver = new ReceiverAdapter(queue);
        var thread =  new ServerThread(receiver);
        var sender = new SenderAdapter(queue);
        thread.Execute();
        var action = new ActionCommand(() => {});

        var sendersPool = IoC.Resolve<ConcurrentDictionary<string, ISender>>("Game.Server.GetSendersPool");
        sendersPool.TryAdd(id, sender);

        var threadsPool = IoC.Resolve<ConcurrentDictionary<string, ServerThread>>("Game.Server.GetThreadsPool");
        threadsPool.TryAdd(id, thread);

        var hss = (Hwdtech.ICommand) new HardStopThreadStrategy().Execute(id, action);
        hss.Execute();
    }

    [Fact]
    public void UnsuccessfulHardStopServerCommand()
    {
        var id = "10";

        var createAndStartSTStrategy = new CreateAndStartThreadStrategy();
        var c = (ServerThread)createAndStartSTStrategy.Execute(id);
        c.Execute();

        var serverThread = threadPool[id];
        var hs = new HardStopServerCommand(id, new ActionCommand(() => {}));

        Assert.Throws<Exception>(() => {
            hs.Execute();
        });

        var hardStopStrategy = new HardStopThreadStrategy();
        var hs1 = (Hwdtech.ICommand)hardStopStrategy.Execute(id);
        hs1.Execute();
    }
}
