using System;
using System.IO;
using System.Collections.Generic;

using Xunit;
using Moq;

using Hwdtech;
using Hwdtech.Ioc;

namespace SpaceBattle.Lib.Test;

public class GenerateObjectStrategyTest {
    [Fact]
    public void successfulGenerateObjectStrategyTest() {

        new InitScopeBasedIoCImplementationCommand().Execute();
        IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"))).Execute();
        
        var countOfObjects = new Mock<IStrategy>();
        countOfObjects.Setup(s => s.Execute()).Returns(5);
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.CountOfObjects", (object[] args) => countOfObjects.Object.Execute(args)).Execute();

        var uobj = new Mock<IUObject>();
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.UObject", (object[] args) => uobj.Object).Execute();

        var gos = new GenerateObjectsStrategy();

        var objects = gos.Execute();
    }
}
