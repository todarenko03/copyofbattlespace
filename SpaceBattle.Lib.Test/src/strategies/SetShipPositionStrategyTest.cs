using System;
using System.Collections.Generic;

using Xunit;
using Moq;

using Hwdtech;
using Hwdtech.Ioc;

namespace SpaceBattle.Lib.Test;

public class SetShipPositionStrategyTest {
    [Fact]
    public void successfulSetShipPositionStrategyTest() {
        
        new InitScopeBasedIoCImplementationCommand().Execute();
        IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"))).Execute();

        var distance = new Mock<IStrategy>();
        distance.Setup(s => s.Execute()).Returns(5);
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.Distance.Ships.Opposition", (object[] args) => distance.Object.Execute()).Execute();

        var positions = new List<Vector>();
        positions.Add(new Vector(new int[] {0, 0}));
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.Positions.Ship", (object[] args) => positions).Execute();
        
        var ind = new Mock<IStrategy>();
        ind.Setup(s => s.Execute()).Returns(2);
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.Index.TempShip", (object[] args) => ind.Object.Execute()).Execute();

        var id = "1";
        var iterator = new PositionEnum();
        var ssps = new SetShipPositionStrategy();
        var sspc = ssps.Execute(id, iterator);
    }
}
