using System;
using System.Collections.Concurrent;

using Xunit;
using Moq;

using Hwdtech;
using Hwdtech.Ioc;
using System.Threading;

namespace SpaceBattle.Lib.Test;

public class CreateAndStartThreadStrategyTest {

    ConcurrentDictionary<string, ServerThread> threadPool = new ConcurrentDictionary<string, ServerThread>();
    ConcurrentDictionary<string, ISender> senderPool = new ConcurrentDictionary<string, ISender>();

    public CreateAndStartThreadStrategyTest() {
        
        new InitScopeBasedIoCImplementationCommand().Execute();
        IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"))).Execute();

        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Server.GetThreadsPool", (object[] args) => threadPool).Execute();
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Server.GetSendersPool", (object[] args) => senderPool).Execute();
    }
    [Fact]
    public void createAndStartThreadStrategyTest1() {
        var casts = new CreateAndStartThreadStrategy();
        var id = "1";
        var thread = casts.Execute(id);

        var hardStopStrategy = new HardStopThreadStrategy();
        var hs = (Hwdtech.ICommand)hardStopStrategy.Execute(id);
        hs.Execute();
    }

    [Fact]
    public void createAndStartThreadStrategyTest2() {
        var casts = new CreateAndStartThreadStrategy();
        var id = "1";
        var action = new ActionCommand(() => {});
        var thread = casts.Execute(id, action);

        var hardStopStrategy = new HardStopThreadStrategy();
        var hs = (Hwdtech.ICommand)hardStopStrategy.Execute(id);
        hs.Execute();
    }

    [Fact]
    public void createStartAndHardStopThreadStrategies()
    {
        var isActive = false;

        var id = "1";

        var hsFlag = false;

        var are = new AutoResetEvent(false);

        var createAndStartSTStrategy = new CreateAndStartThreadStrategy();
        var c = (ServerThread)createAndStartSTStrategy.Execute(id);
        c.Execute();

        var sendStrategy = new SendCommandStrategy();
        var c1 = (Hwdtech.ICommand)sendStrategy.Execute(id, new ActionComand(() =>
        {
            isActive = true;
            are.Set();
        }));
        c1.Execute();

        are.WaitOne();

        Assert.True(isActive);
        Assert.True(threadPool.TryGetValue(id, out ServerThread? st));
        Assert.True(senderPool.TryGetValue(id, out ISender? s));

        var hardStopStrategy = new HardStopThreadStrategy();
        var hs = (Hwdtech.ICommand)hardStopStrategy.Execute(id, new ActionCommand(() =>
        {
            hsFlag = true;
            are.Set();
        }));

        hs.Execute();
        are.WaitOne();

        Assert.True(hsFlag);
    }
}
