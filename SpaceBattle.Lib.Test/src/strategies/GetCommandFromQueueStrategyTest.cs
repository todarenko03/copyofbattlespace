using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace SpaceBattle.Lib.Test;

public class GetCommandFromQueueStrategyTest {
    [Fact]
    public void SuccessfullGetCommandFromQueue() {
        var cmd = new Mock<ICommand>();
        var queue = new Queue<ICommand>();
        queue.Enqueue(cmd.Object);

        var getCommandFromQueueStrategy = new GetCommandFromQueueStrategy();
        var cmdFromQueue = getCommandFromQueueStrategy.Execute(queue);
        
        Assert.True(cmd.Object == cmdFromQueue);
        Assert.True(queue.Count == 0);
    }
}
