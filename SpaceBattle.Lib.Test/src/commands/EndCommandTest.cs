using System;
using System.Collections.Generic;

using Xunit;
using Moq;

using Hwdtech;
using Hwdtech.Ioc;

namespace SpaceBattle.Lib.Test {
    public class EndableCommandTest {
        public EndableCommandTest() {
            new InitScopeBasedIoCImplementationCommand().Execute();
            IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"))).Execute();

            var propertiesDeletable = new Mock<IPropertiesDeletable>();
            var uobject = new Mock<IUObject>();
            propertiesDeletable.Setup(p => p.DeleteProperties(uobject.Object, It.IsAny<IList<string>>()));
            var propertiesDeleteStrategy = new PropertiesDeleteStrategy(propertiesDeletable.Object);
            IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Object.DeleteProperties", (object[] args) => (propertiesDeleteStrategy.Execute(args))).Execute();

            var injectStrategy = new InjectStrategy();
            IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Commands.Inject", (object[] args) => (injectStrategy.Execute(args))).Execute();

            var queuePushStrategy= new Mock<IStrategy>();
            queuePushStrategy.Setup(s => s.Execute(It.IsAny<object[]>()));
            IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Queue.Push", (object[] args) => (queuePushStrategy.Object.Execute(args))).Execute();
        }

        [Fact]
        public void EndCommandTest(){
            var endable = new Mock<IEndable>();
            endable.SetupGet<IUObject>(m => m.UObject).Returns(new Mock<IUObject>().Object);
            endable.SetupGet<ICommand>(m => m.Command).Returns(new Mock<ICommand>().Object);
            endable.SetupGet<IList<string>>(m => m.Properties).Returns(new Mock<IList<string>>().Object);

            var endCommand = new EndCommand(endable.Object);
            endCommand.Execute();
        }

        [Fact]
        public void UnreadableCommandTest(){
            var endable = new Mock<IEndable>();
            endable.SetupGet<IUObject>(m => m.UObject).Returns(new Mock<IUObject>().Object);
            endable.SetupGet<ICommand>(m => m.Command).Throws<Exception>();
            endable.SetupGet<IList<string>>(m => m.Properties).Returns(new Mock<IList<string>>().Object);

            var endCommand = new EndCommand(endable.Object);

            Assert.Throws<Exception>(() => endCommand.Execute());
        }

        [Fact]
        public void UnreadableObjectTest(){
            var endable = new Mock<IEndable>();
            endable.SetupGet<IUObject>(m => m.UObject).Throws<Exception>();
            endable.SetupGet<ICommand>(m => m.Command).Returns(new Mock<ICommand>().Object);
            endable.SetupGet<IList<string>>(m => m.Properties).Returns(new Mock<IList<string>>().Object);

            var endCommand = new EndCommand(endable.Object);

            Assert.Throws<Exception>(() => endCommand.Execute());
        }

        [Fact]
        public void UnreadablePropertiesTest(){
            var endable = new Mock<IEndable>();
            endable.SetupGet<IUObject>(m => m.UObject).Returns(new Mock<IUObject>().Object);
            endable.SetupGet<ICommand>(m => m.Command).Returns(new Mock<ICommand>().Object);
            endable.SetupGet<IList<string>>(m => m.Properties).Throws<Exception>();

            var endCommand = new EndCommand(endable.Object);

            Assert.Throws<Exception>(() => endCommand.Execute());
        }
    }
}
