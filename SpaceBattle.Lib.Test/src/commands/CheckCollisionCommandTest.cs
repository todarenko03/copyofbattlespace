using System;
using System.Collections.Generic;

using Xunit;
using Moq;

using Hwdtech;
using Hwdtech.Ioc;

namespace SpaceBattle.Lib.Test {
    public class CheckCollisionCommandTest {
        public CheckCollisionCommandTest() {
            new InitScopeBasedIoCImplementationCommand().Execute();
            IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"))).Execute();

            var getPositionStrategy = new GetPositionStrategy();
            IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Object.GetPosition", (object[] args) => (getPositionStrategy.Execute(args))).Execute();

            var createSolutionTreeStrategy = new CreateSolutionTreeStrategy();
            IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Collision.CreateSolutionTree", (object[] args) => (createSolutionTreeStrategy.Execute(args))).Execute();

            var getSolutionTreeDataStrategy = new GetSolutionTreeDataStrategy();
            IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Collision.GetSolutionTreeData", (object[] args) => (getSolutionTreeDataStrategy.Execute(args))).Execute();

            var collisionFoundStrategy = new CollisionFoundStrategy();
            IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Collision.CollisionFound", (object[] args) => (collisionFoundStrategy.Execute(args))).Execute();
        }

        [Fact]
        public void CollisionFoundTest() {
            var a = new Mock<IUObject>();
            var b = new Mock<IUObject>();

            a.Setup(o => o.GetProperty("Position")).Returns(new List<int>{0, 0, 0, 0});
            b.Setup(o => o.GetProperty("Position")).Returns(new List<int>{0, 0, 0, 0});

            var checkCollisionCommand = new CheckCollisionCommand(a.Object, b.Object);
            Assert.Throws<Exception>(() => checkCollisionCommand.Execute());
        }

        [Fact]
        public void CollisionNotFoundTest() {
            var a = new Mock<IUObject>();
            var b = new Mock<IUObject>();

            a.Setup(o => o.GetProperty("Position")).Returns(new List<int>{0, 0, 0, 0});
            b.Setup(o => o.GetProperty("Position")).Returns(new List<int>{1, 2, 3, 4});

            var checkCollisionCommand = new CheckCollisionCommand(a.Object, b.Object);
            checkCollisionCommand.Execute();
        }
    }
}
