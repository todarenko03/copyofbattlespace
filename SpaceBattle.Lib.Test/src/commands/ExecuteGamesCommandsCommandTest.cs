using Moq;
using Hwdtech.Ioc;
using Hwdtech;
using System.Collections.Generic;
using Xunit;
using System.Threading;
using System;

namespace SpaceBattle.Lib.Test;

public class ExecuteGamesCommandsCommandTest{

    [Fact]
    public void SuccessfulExecuteGamesCommandsCommand() {

        new InitScopeBasedIoCImplementationCommand().Execute();
        IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"))).Execute();


        var getCommandFromQueue = new GetCommandFromQueueStrategy();
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.CommandFromQueue", (object[] args) => getCommandFromQueue.Execute(args)).Execute(); 

        var exceptionHandleStrategy = new Mock<IStrategy>();
        var exceptionHandler = new Mock<IHandler>();
        exceptionHandler.Setup(h => h.Handle()).Callback(() => {});
        exceptionHandleStrategy.Setup(s => s.Execute(It.IsAny<object[]>())).Returns(exceptionHandler.Object);

        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.ExceptionHandler", (object[] args) => exceptionHandleStrategy.Object.Execute(args)).Execute();

        var queue = new Queue<ICommand>();

        queue.Enqueue(new ActionCommand(() => {

            IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"))).Execute();

            var getQuantumOfTimeStrategy = new Mock<IStrategy>();
            getQuantumOfTimeStrategy.Setup(s => s.Execute()).Returns(0.0);
            IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.QuantumOfTime", (object[] args) => getQuantumOfTimeStrategy.Object.Execute(args)).Execute();
        }
        ));
        queue.Enqueue(new ActionCommand(() => {}));
        queue.Enqueue(new ActionCommand(() => {}));

        var getQuantumOfTimeStrategy = new Mock<IStrategy>();
        getQuantumOfTimeStrategy.Setup(s => s.Execute()).Returns(333.0);
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.QuantumOfTime", (object[] args) => getQuantumOfTimeStrategy.Object.Execute(args)).Execute();

        Assert.True(IoC.Resolve<double>("Game.Get.QuantumOfTime") == 333.0);

        var executeGamesCommandsCommand = new ExecuteGamesCommandsCommand(queue);
        executeGamesCommandsCommand.Execute();

        Assert.True(IoC.Resolve<double>("Game.Get.QuantumOfTime") == 0.0);

        Assert.True(queue.Count == 2);
    }

    [Fact]
    public void UnsuccessfulExecuteGamesCommandsCommand() {

        new InitScopeBasedIoCImplementationCommand().Execute();
        IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"))).Execute();

        var getCommandFromQueue = new GetCommandFromQueueStrategy();
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.CommandFromQueue", (object[] args) => getCommandFromQueue.Execute(args)).Execute(); 

        var exceptionHandleStrategy = new Mock<IStrategy>();
        var exceptionHandler = new Mock<IHandler>();
        exceptionHandleStrategy.Setup(s => s.Execute(It.IsAny<object[]>())).Returns(exceptionHandler.Object);
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.ExceptionHandler", (object[] args) => exceptionHandleStrategy.Object.Execute(args)).Execute();

        var queue = new Queue<ICommand>();

        queue.Enqueue(new ActionCommand(() => {}));

        var getQuantumOfTimeStrategy = new Mock<IStrategy>();
        getQuantumOfTimeStrategy.Setup(s => s.Execute()).Returns(333.0);
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.QuantumOfTime", (object[] args) => getQuantumOfTimeStrategy.Object.Execute(args)).Execute();

        var unexecutableCmd = new Mock<ICommand>();
        unexecutableCmd.Setup(c => c.Execute()).Callback(() => new Exception());

        var executeGamesCommandsCommand = new ExecuteGamesCommandsCommand(queue);
        Assert.Throws<Exception>(() => executeGamesCommandsCommand.Execute());
    }
    [Fact]
    public void unexecutableCommandTest() {
        new InitScopeBasedIoCImplementationCommand().Execute();
        IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"))).Execute();

        var getCommandFromQueue = new GetCommandFromQueueStrategy();
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.CommandFromQueue", (object[] args) => getCommandFromQueue.Execute(args)).Execute(); 

        var getQuantumOfTimeStrategy = new Mock<IStrategy>();
        getQuantumOfTimeStrategy.Setup(s => s.Execute()).Returns(333.0);
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.QuantumOfTime", (object[] args) => getQuantumOfTimeStrategy.Object.Execute(args)).Execute();

        var exceptionHandleStrategy = new Mock<IStrategy>();
        var exceptionHandler = new Mock<IHandler>();
        exceptionHandleStrategy.Setup(s => s.Execute(It.IsAny<object[]>())).Returns(exceptionHandler.Object);
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.ExceptionHandler", (object[] args) => exceptionHandleStrategy.Object.Execute(args)).Execute();

        var queue = new Queue<ICommand>();

        var unexecutableCmd = new Mock<ICommand>();
        unexecutableCmd.Setup(c => c.Execute()).Throws<Exception>();

        queue.Enqueue(unexecutableCmd.Object);


        var executeGamesCommandsCommand = new ExecuteGamesCommandsCommand(queue);
        Assert.Throws<Exception>(() => executeGamesCommandsCommand.Execute());
    }
}
