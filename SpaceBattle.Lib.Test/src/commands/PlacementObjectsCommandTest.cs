using System.Collections.Generic;

using Hwdtech;
using Hwdtech.Ioc;

using Xunit;
using Moq;

namespace SpaceBattle.Lib.Test;

public class PlacementObjectsCommandTest {
    [Fact]
    public void successfulPlacementObjectsCommandTest() {
        
        new InitScopeBasedIoCImplementationCommand().Execute();
        IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"))).Execute();

        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.PositionEnum", (object[] args) => new PositionEnum()).Execute();

        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Set.Ship.Position", (object[] args) => new SetShipPositionStrategy().Execute(args)).Execute();

        var distance = new Mock<IStrategy>();
        distance.Setup(s => s.Execute()).Returns(5);
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.Distance.Ships.Opposition", (object[] args) => distance.Object.Execute()).Execute();

        var positions = new List<Vector>();
        positions.Add(new Vector(new int[] {0, 0}));
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.Positions.Ship", (object[] args) => positions).Execute();
        
        var ind = new Mock<IStrategy>();
        ind.Setup(s => s.Execute()).Returns(0);
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.Index.TempShip", (object[] args) => ind.Object.Execute()).Execute();

        var dist = new Mock<IStrategy>();
        dist.Setup(s => s.Execute()).Returns(4);
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.Distance.Ships.Team", (object[] args) => dist.Object.Execute()).Execute();

        var uobj = new Mock<IUObject>();
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.UObject", (object[] args) => uobj.Object).Execute();

        var dict = new Dictionary<string, IUObject>();
        dict.Add("1", new Mock<IUObject>().Object);
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.Objects", (object[] args) => dict).Execute();

        var koef = new Mock<IStrategy>();
        koef.Setup(s => s.Execute()).Returns(1);
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.Coefficient.ShipsPlace", (object[] args) => koef.Object.Execute()).Execute();


        var shipID = new List<string>();

        shipID.Add("1");

        var poc = new PlacementObjectsCommand(shipID);

        poc.Execute();
    }
}
