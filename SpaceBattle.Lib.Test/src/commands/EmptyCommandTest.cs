using System;

using Xunit;
using Moq;

namespace SpaceBattle.Lib.Test {
    public class EmptyCommandTest {
        [Fact]
        public void emptyCommandTest(){
            var emptyCommand = new EmptyCommand();
            emptyCommand.Execute();
        }
    }
}
