using Moq;
using Hwdtech.Ioc;
using Hwdtech;
using System.Collections.Generic;
using Xunit;

namespace SpaceBattle.Lib.Test;

public class GameCommandTest {

    [Fact]
    public void SuccessfulGameCommandExecuteTest() {
        new InitScopeBasedIoCImplementationCommand().Execute();
        var scope = IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"));
        IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", scope).Execute();

        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.QuantumOfTime", (object[] args) => (object)5.0).Execute();

        var cmd = new Mock<ICommand>();
        cmd.Setup(c => c.Execute()).Callback(() => {});
        
        var executeGamesCommandsStrategy = new Mock<IStrategy>();
        executeGamesCommandsStrategy.Setup(s => s.Execute(It.IsAny<object[]>())).Returns(cmd.Object);
        
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Execute.GamesCommands", (object[] args) => executeGamesCommandsStrategy.Object.Execute(args)).Execute();

        var queue = new Queue<ICommand>();

        var game = new GameCommand(queue, scope);

        game.Execute();

        Assert.True(scope == IoC.Resolve<object>("Scopes.Current"));
    }
}
