using System;
using System.IO;
using System.Collections.Generic;

using Xunit;
using Moq;

using Hwdtech;
using Hwdtech.Ioc;

namespace SpaceBattle.Lib.Test;

public class SetFuelCommandTest {
    [Fact]
    public void successfulSetFuelCommandTest() {

        new InitScopeBasedIoCImplementationCommand().Execute();
        IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"))).Execute();

        var dict = new Dictionary<string, IUObject>();
        dict.Add("1", new Mock<IUObject>().Object);
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Get.Objects", (object[] args) => dict).Execute();

        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Set.Property.UObject", (object[] args) => new Mock<ICommand>().Object).Execute();

        var shipIDForSetting = "1";
        var fuelValue = 3;
        var sfc = new SetFuelCommand(shipIDForSetting, fuelValue);
        sfc.Execute();
    }
}
