using System;
using System.Collections.Generic;

using Xunit;
using Moq;

using Hwdtech;
using Hwdtech.Ioc;

namespace SpaceBattle.Lib.Test {
    public class StartCommandTest {
        public StartCommandTest() {
            new InitScopeBasedIoCImplementationCommand().Execute();
            IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"))).Execute();

            var propertiesSetable = new Mock<IPropertiesSetable>();
            var uobject = new Mock<IUObject>();
            propertiesSetable.Setup(p => p.SetProperties(uobject.Object, It.IsAny<IList<string>>()));

            var propertiesSetStrategy = new PropertiesSetStrategy(propertiesSetable.Object);
            IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Object.SetProperties", (object[] args) => (propertiesSetStrategy.Execute(args))).Execute();

            var repeatStrategy = new RepeatStrategy();
            IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Commands.Repeat", (object[] args) => (repeatStrategy.Execute(args))).Execute();

            var queuePushStrategy = new Mock<IStrategy>();
            queuePushStrategy.Setup(p => p.Execute(It.IsAny<object[]>()));
            IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Queue.Push", (object[] args) => (queuePushStrategy.Object.Execute(args))).Execute();
        }

        [Fact]
        public void StartableCommandTest() {
            var startable = new Mock<IStartable>();
            startable.SetupGet<IUObject>(m => m.UObject).Returns(new Mock<IUObject>().Object);
            startable.SetupGet<ICommand>(m => m.Command).Returns(new Mock<ICommand>().Object);
            startable.SetupGet<IList<string>>(m => m.Properties).Returns(new Mock<IList<string>>().Object);

            var startCommand = new StartCommand(startable.Object);
            startCommand.Execute();
        }

        [Fact]
        public void UnreadableCommandTest() {
            var startable = new Mock<IStartable>();
            startable.SetupGet<ICommand>(m => m.Command).Throws<Exception>();
            startable.SetupGet<IUObject>(m => m.UObject).Returns(new Mock<IUObject>().Object);
            startable.SetupGet<IList<string>>(m => m.Properties).Returns(new Mock<IList<string>>().Object);

            var startCommand = new StartCommand(startable.Object);

            Assert.Throws<Exception>(() => startCommand.Execute());
        }

        [Fact]
        public void UnreadableObjectTest() {
            var startable = new Mock<IStartable>();
            startable.SetupGet<ICommand>(m => m.Command).Returns(new Mock<ICommand>().Object);
            startable.SetupGet<IUObject>(m => m.UObject).Throws<Exception>();
            startable.SetupGet<IList<string>>(m => m.Properties).Returns(new Mock<IList<string>>().Object);

            var startCommand = new StartCommand(startable.Object);

            Assert.Throws<Exception>(() => startCommand.Execute());
        }

        [Fact]
        public void UnreadablePropertiesTest() {
            var startable = new Mock<IStartable>();
            startable.SetupGet<ICommand>(m => m.Command).Returns(new Mock<ICommand>().Object);
            startable.SetupGet<IUObject>(m => m.UObject).Returns(new Mock<IUObject>().Object);
            startable.SetupGet<IList<string>>(m => m.Properties).Throws<Exception>();

            var startCommand = new StartCommand(startable.Object);

            Assert.Throws<Exception>(() => startCommand.Execute());
        }
    }
}
