using System;

using Xunit;
using Moq;

namespace SpaceBattle.Lib.Test {
    public class RotatementTest{
        [Fact]
        public void ChangeAngleTest() {
            var rotatable = new Mock<IRotatable>();
            rotatable.Setup(m => m.Angle).Returns(45);
            rotatable.Setup(m => m.AngularVelocity).Returns(90);
            
            var rotateCommand = new RotateCommand(rotatable.Object);
            rotateCommand.Execute();

            rotatable.VerifySet(m => m.Angle = 135);
        }

        [Fact]
        public void UnreadableAngleTest(){
            var rotatable = new Mock<IRotatable>();
            rotatable.SetupGet(m => m.Angle).Throws<Exception>();
            rotatable.SetupGet(m => m.AngularVelocity).Returns(90);

            var rotateCommand = new RotateCommand(rotatable.Object);
            Assert.Throws<Exception>(() => rotateCommand.Execute());
        }

        [Fact]
        public void UnreadableAngleVelocityTest(){
            var rotatable = new Mock<IRotatable>();
            rotatable.SetupProperty(m => m.Angle, 45);
            rotatable.SetupGet(m => m.AngularVelocity).Throws<Exception>();

            var rotateCommand = new RotateCommand(rotatable.Object);
            Assert.Throws<Exception>(() => rotateCommand.Execute());
        }

        [Fact]
        public void ImmutableAngleTest(){
            var rotatable = new Mock<IRotatable>();
            rotatable.SetupProperty(m => m.Angle, 45);
            rotatable.SetupGet(m => m.AngularVelocity).Returns(90);
            rotatable.SetupSet(m => m.Angle = It.IsAny<int>()).Throws<Exception>();   
            
            var rotateCommand = new RotateCommand(rotatable.Object);
            Assert.Throws<Exception>(() => rotateCommand.Execute());
        }
    }
}
