using System.Threading;
using System.Collections.Concurrent;
using Xunit;
using Moq;
using System;

using Hwdtech;
using Hwdtech.Ioc;

namespace SpaceBattle.Lib.Test;

public class ServerThreadTest {

    public ServerThreadTest() {

        var threadPool = new ConcurrentDictionary<string, ServerThread>();
        var senderPool = new ConcurrentDictionary<string, ISender>();

        new InitScopeBasedIoCImplementationCommand().Execute();
        IoC.Resolve<Hwdtech.ICommand>("Scopes.Current.Set", IoC.Resolve<object>("Scopes.New", IoC.Resolve<object>("Scopes.Root"))).Execute();

        var createAndStartThreadStrategy = new CreateAndStartThreadStrategy();
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Server.CreateThread", (object[] args) => createAndStartThreadStrategy.Execute(args)).Execute();

        var sendCommandStrategy = new SendCommandStrategy();
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Server.SendCommand", (object[] args) => sendCommandStrategy.Execute(args)).Execute();

        var softStopThreadStrategy = new SoftStopThreadStrategy();
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Server.SoftStopThread", (object[] args) => softStopThreadStrategy.Execute(args)).Execute();

        var hardStopThreadStrategy = new HardStopThreadStrategy();
        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Server.HardStopThread", (object[] args) => hardStopThreadStrategy.Execute(args)).Execute();

        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Server.GetThreadsPool", (object[] args) => threadPool).Execute();

        IoC.Resolve<Hwdtech.ICommand>("IoC.Register", "Game.Server.GetSendersPool", (object[] args) => senderPool).Execute();
    }
    [Fact]
    public void createAndStartThreadTest(){
        var id = "0";
        IoC.Resolve<ServerThread>("Game.Server.CreateThread", id).Execute();
    
        var hardStopStrategy = new HardStopThreadStrategy();
        var hs = (Hwdtech.ICommand)hardStopStrategy.Execute(id);
        hs.Execute();
    }

    [Fact]
    public void sendStartMoveCommand() {
        var id = "2";
        IoC.Resolve<ServerThread>("Game.Server.CreateThread", id).Execute();
        var are = new AutoResetEvent(true);
        var cmd = new ActionComand(() => {are.Set();});
        
        are.WaitOne();

        IoC.Resolve<Hwdtech.ICommand>("Game.Server.SendCommand", id, cmd).Execute();

        var threadsPool = IoC.Resolve<ConcurrentDictionary<string, ServerThread>>("Game.Server.GetThreadsPool");
        var thread = threadsPool[id];

        are.WaitOne();

        var hardStopStrategy = new HardStopThreadStrategy();
        var hs = (Hwdtech.ICommand)hardStopStrategy.Execute(id);
        hs.Execute();
    }

    [Fact]
    public void UnsuccessfulHardStopServerThreadFromOtherThread() {

        var id = "9";
        IoC.Resolve<ServerThread>("Game.Server.CreateThread", id).Execute();
        var action = new ActionCommand(() => {});

        var hs = new HardStopServerCommand(id, action);

        Assert.Throws<Exception>(() => {
            hs.Execute();
        });

        var hardStopStrategy = new HardStopThreadStrategy();
        var hs2 = (Hwdtech.ICommand)hardStopStrategy.Execute(id);
        hs2.Execute();
    }

    [Fact]
    public void ServerThreadGetHashCodeTest()
    {
        var queue1 = new BlockingCollection<Hwdtech.ICommand>();
        var serverThread1 = new ServerThread(new ReceiverAdapter(queue1));
        var queue2 = new BlockingCollection<Hwdtech.ICommand>();
        var serverThread2 = new ServerThread(new ReceiverAdapter(queue2));
        Assert.True(serverThread1.GetHashCode() != serverThread2.GetHashCode());
    }

    [Fact]
    public void ServerThreadEqualsIsNotThreadTest()
    {
        var queue1 = new BlockingCollection<Hwdtech.ICommand>();
        var serverThread1 = new ServerThread(new ReceiverAdapter(queue1));
        Assert.False(serverThread1.Equals(2));
    }

    [Fact]
    public void ServerThreadEqualsTest()
    {
        var queue1 = new BlockingCollection<Hwdtech.ICommand>();
        var serverThread1 = new ServerThread(new ReceiverAdapter(queue1));
        Assert.False(serverThread1.Equals(Thread.CurrentThread));
    }

    [Fact]
    public void ServerThreadOperatorEqualsTest1()
    {
        var queue1 = new BlockingCollection<Hwdtech.ICommand>();
        var serverThread1 = new ServerThread(new ReceiverAdapter(queue1));
        Assert.True(serverThread1 != Thread.CurrentThread);
    }
    [Fact]
    public void ServerThreadOperatorEqualsTest2()
    {
        var queue1 = new BlockingCollection<Hwdtech.ICommand>();
        var serverThread1 = new ServerThread(new ReceiverAdapter(queue1));
        Assert.False(serverThread1 == Thread.CurrentThread);
    }
}
