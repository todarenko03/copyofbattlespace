using System;

using Xunit;

namespace SpaceBattle.Lib.Test {
    public class VectorTest {
        [Fact]
        public void ConstructorTest() {
            var vector = new Vector(1, 2, 3);
            var verifyVector = new Vector(1, 2, 3);

            Assert.Equal(vector, verifyVector);
        }

        [Fact]
        public void GetterIndexTest() {
            var vector = new Vector(1, 2, 3);
            var verifyValue= 1;

            Assert.Equal(vector[0], verifyValue);
        }

        [Fact]
        public void SetterIndexTest() {
            var vector = new Vector(1, 2, 3);
            var verifyValue = 5;

            vector[0] = 5;

            Assert.Equal(vector[0], verifyValue);
        }

        [Fact]
        public void GetSizeTest() {
            var vector = new Vector(1, 2, 3);
            var verifySize = 3;

            Assert.Equal(vector.Size(), verifySize);
        }

        [Fact]
        public void SameSizeVectorsAdditionTest() {
            var vectorA= new Vector(1, 2, 3);
            var vectorB = new Vector(4, 5, 6);
            var verifyVector = new Vector(5, 7, 9);

            Assert.Equal(vectorB + vectorB, verifyVector);
        }

        [Fact]
        public void DifferentSizeVectorsAdditionTest(){
            var vectorA = new Vector(1, 2, 3);
            var vectorB = new Vector(4, 5, 6, 7);

            Assert.Throws<ArgumentException>(() => vectorA + vectorB);
        }

        [Fact]
        public void SameSizeVectorsDifferenceTest() {
            var vectorA = new Vector(1, 2, 3);
            var vectorB = new Vector(4, 5, 6);
            var verifyVector = new Vector(-3, -3, -3);

            Assert.Equal(vectorA - vectorB, verifyVector);
        }

        [Fact]
        public void DifferentSizeVectorsDifferenceTest(){
            var vectorA = new Vector(1, 2, 3);
            var vectorB = new Vector(4, 5, 6, 7);

            Assert.Throws<ArgumentException>(() => vectorA - vectorB);
        }

        [Fact]
        public void GetHashCodeTest() {
            Vector A = new Vector(0, 1, 0, 1);
            Vector B = new Vector(0, 1, 0, 1);
            Assert.Equal(A.GetHashCode(), B.GetHashCode());
        }
    }
}
